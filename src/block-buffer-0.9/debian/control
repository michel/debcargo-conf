Source: rust-block-buffer-0.9
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-generic-array-0.14+default-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Jochen Sprickerhof <jspricke@debian.org>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/block-buffer-0.9]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/block-buffer-0.9
Rules-Requires-Root: no

Package: librust-block-buffer-0.9-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-generic-array-0.14+default-dev
Suggests:
 librust-block-buffer-0.9+block-padding-dev (= ${binary:Version})
Provides:
 librust-block-buffer-dev (= ${binary:Version}),
 librust-block-buffer+default-dev (= ${binary:Version}),
 librust-block-buffer-0-dev (= ${binary:Version}),
 librust-block-buffer-0+default-dev (= ${binary:Version}),
 librust-block-buffer-0.9+default-dev (= ${binary:Version}),
 librust-block-buffer-0.9.0-dev (= ${binary:Version}),
 librust-block-buffer-0.9.0+default-dev (= ${binary:Version})
Replaces: librust-block-buffer-dev (<< 0.9.1)
Breaks: librust-block-buffer-dev (<< 0.9.1)
Description: Fixed size buffer for block processing of data - Rust source code
 This package contains the source for the Rust block-buffer crate, packaged by
 debcargo for use with cargo and dh-cargo.

Package: librust-block-buffer-0.9+block-padding-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-block-buffer-0.9-dev (= ${binary:Version}),
 librust-block-padding-0.2+default-dev
Provides:
 librust-block-buffer+block-padding-dev (= ${binary:Version}),
 librust-block-buffer-0+block-padding-dev (= ${binary:Version}),
 librust-block-buffer-0.9.0+block-padding-dev (= ${binary:Version})
Description: Fixed size buffer for block processing of data - feature "block-padding"
 This metapackage enables feature "block-padding" for the Rust block-buffer
 crate, by pulling in any additional dependencies needed by that feature.

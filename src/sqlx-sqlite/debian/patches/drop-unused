Index: sqlx-sqlite/src/statement/virtual.rs
===================================================================
--- sqlx-sqlite.orig/src/statement/virtual.rs
+++ sqlx-sqlite/src/statement/virtual.rs
@@ -5,9 +5,7 @@ use std::ptr::{null, null_mut, NonNull};
 use std::sync::Arc;
 use std::{cmp, i32};
 
-use libsqlite3_sys::{
-    sqlite3, sqlite3_prepare_v3, sqlite3_stmt, SQLITE_OK, SQLITE_PREPARE_PERSISTENT,
-};
+use libsqlite3_sys::{sqlite3, sqlite3_prepare_v2, sqlite3_stmt, SQLITE_OK};
 
 use sqlx_core::bytes::{Buf, Bytes};
 use sqlx_core::error::Error;
@@ -145,16 +143,6 @@ fn prepare(
     query: &mut Bytes,
     persistent: bool,
 ) -> Result<Option<StatementHandle>, Error> {
-    let mut flags = 0;
-
-    if persistent {
-        // SQLITE_PREPARE_PERSISTENT
-        //  The SQLITE_PREPARE_PERSISTENT flag is a hint to the query
-        //  planner that the prepared statement will be retained for a long time
-        //  and probably reused many times.
-        flags |= SQLITE_PREPARE_PERSISTENT;
-    }
-
     while !query.is_empty() {
         let mut statement_handle: *mut sqlite3_stmt = null_mut();
         let mut tail: *const c_char = null();
@@ -164,14 +152,7 @@ fn prepare(
 
         // <https://www.sqlite.org/c3ref/prepare.html>
         let status = unsafe {
-            sqlite3_prepare_v3(
-                conn,
-                query_ptr,
-                query_len,
-                flags as u32,
-                &mut statement_handle,
-                &mut tail,
-            )
+            sqlite3_prepare_v2(conn, query_ptr, query_len, &mut statement_handle, &mut tail)
         };
 
         if status != SQLITE_OK {

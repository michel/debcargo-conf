rust-debcargo (2.6.0-3) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.6.0 from crates.io using debcargo 2.6.0
  * Add patch for env-logger 0.10

 -- Matthias Geiger <werdahias@riseup.net>  Thu, 10 Aug 2023 17:43:57 +0200

rust-debcargo (2.6.0-2) unstable; urgency=medium

  * Team upload.
  * Rebuild debcargo 2.6.0 with cargo 0.66.0

 -- Fabian Gruenbichler <debian@fabian.gruenbichler.email>  Thu, 12 Jan 2023 16:33:49 +0000

rust-debcargo (2.6.0-1) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.6.0 from crates.io using debcargo 2.5.0

 -- Fabian Gruenbichler <debian@fabian.gruenbichler.email>  Wed, 16 Nov 2022 10:08:41 +0100

rust-debcargo (2.5.0-5) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.5.0 from crates.io using debcargo 2.5.0
  * Bump textwrap dependency.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 30 Oct 2022 16:37:02 +0000

rust-debcargo (2.5.0-4) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.5.0 from crates.io using debcargo 2.5.0
  * Bump git2 dependency.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 08 Oct 2022 04:27:58 +0000

rust-debcargo (2.5.0-3) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.5.0 from crates.io using debcargo 2.5.0
  * Update dependencies for new textwrap version in Debian.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 14 Mar 2022 01:24:43 +0000

rust-debcargo (2.5.0-2) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.5.0 from crates.io using debcargo 2.5.0
  * Don't run the build-time tests on mips64el as they fail to link.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 29 Nov 2021 11:57:39 +0000

rust-debcargo (2.5.0-1) unstable; urgency=medium

  * Package debcargo 2.5.0 from crates.io using debcargo 2.5.0

 -- Ximin Luo <infinity0@debian.org>  Thu, 11 Nov 2021 13:35:58 +0000

rust-debcargo (2.4.4-1) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.4.4 from crates.io using debcargo 2.4.3
  * Adjust dependencies for versions currently in sid.
  * Revert cargo 0.49 related changes until rust-cargo is
    updated in Debian.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 06 Feb 2021 18:12:33 +0000

rust-debcargo (2.4.3-3) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.4.3 from crates.io using debcargo 2.4.3
  * Bump dependency on git2 package, the old version is broken
    with new libgit2 (Closes: 976658).

 -- Peter Michael Green <plugwash@debian.org>  Mon, 07 Dec 2020 05:21:57 +0000

rust-debcargo (2.4.3-2) unstable; urgency=medium

  * Team upload.
  * Package debcargo 2.4.3 from crates.io using debcargo 2.4.2
  * Relax dependency on itertools

 -- Peter Michael Green <plugwash@debian.org>  Tue, 01 Sep 2020 13:47:42 +0000

rust-debcargo (2.4.3-1) unstable; urgency=medium

  * Package debcargo 2.4.3 from crates.io using debcargo 2.4.2

 -- Ximin Luo <infinity0@debian.org>  Sat, 18 Apr 2020 20:52:06 +0100

rust-debcargo (2.4.2-1) unstable; urgency=medium

  * Package debcargo 2.4.2 from crates.io using debcargo 2.4.2

 -- Ximin Luo <infinity0@debian.org>  Wed, 08 Jan 2020 21:54:13 +0000

rust-debcargo (2.4.1-1) unstable; urgency=medium

  * Package debcargo 2.4.1 from crates.io using debcargo 2.4.1
  * Closes: #945436, #945499, #945560, #947088.

 -- Ximin Luo <infinity0@debian.org>  Fri, 03 Jan 2020 01:19:34 +0000

rust-debcargo (2.4.0-1) unstable; urgency=medium

  * Package debcargo 2.4.0 from crates.io using debcargo 2.4.0
    (Closes: #931897)

 -- Ximin Luo <infinity0@debian.org>  Thu, 15 Aug 2019 19:07:57 -0700

rust-debcargo (2.2.10-1) unstable; urgency=medium

  * Package debcargo 2.2.10 from crates.io using debcargo 2.2.10

 -- Ximin Luo <infinity0@debian.org>  Sun, 20 Jan 2019 23:25:15 -0800

rust-debcargo (2.2.9-2) unstable; urgency=medium

  * Package debcargo 2.2.9 from crates.io using debcargo 2.2.9
  * Add dependency on quilt.
  * Install debcargo.toml.example as an example.

 -- Ximin Luo <infinity0@debian.org>  Thu, 29 Nov 2018 22:33:36 -0800

rust-debcargo (2.2.9-1) unstable; urgency=medium

  * Package debcargo 2.2.9 from crates.io using debcargo 2.2.9

 -- Ximin Luo <infinity0@debian.org>  Wed, 14 Nov 2018 21:03:02 -0800

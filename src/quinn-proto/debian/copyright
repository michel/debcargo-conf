Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: quinn-proto
Upstream-Contact: Dirkjan Ochtman <dirkjan@ochtman.nl>
                  Benjamin Saunders <ben.e.saunders@gmail.com>
                  Jean-Christophe Begue <begue.jc@gmail.com>
Source: https://github.com/quinn-rs/quinn

Files: *
Copyright: 2018-2022 Dirkjan Ochtman <dirkjan@ochtman.nl>
           2018-2022 Benjamin Saunders <ben.e.saunders@gmail.com>
           2018-2022 Jean-Christophe Begue <begue.jc@gmail.com> 
License: MIT or Apache-2.0
Comment: The license mentions the Quinn Developers, those are the Copyright holders/Upstream-Contacts.

Files: src/congestion/bbr/mod.rs
Copyright: 2015 The Chromium Authors
License: BSD-3-Clause
Comment: Part of this file is based on https://source.chromium.org/chromium/chromium/src/+/main:net/third_party/quiche/src/quiche/quic/core/congestion_control/bbr_sender.cc

Files: src/congestion/bbr/min_max.rs
Copyright: 2016 The Chromium Authors
License: BSD-3-Clause
Comment: Part of this file is based on a Google BSD-Licensed file which implements the min_max function without using the GPL code. It can be found here:
 https://source.chromium.org/chromium/chromium/src/+/main:net/third_party/quiche/src/quiche/quic/core/congestion_control/windowed_filter.h .
 The license mentions https://groups.google.com/g/bbr-dev/c/3RTgkzi5ZD8 as spurce wich references windowed_filter.h

Files: debian/* 
Copyright:
 2023 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2023 Matthias Geiger <matthias.geiger1024@tutanota.de>
License: MIT or Apache-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following disclaimer
 in the documentation and/or other materials provided with the
 distribution.
 * Neither the name of Google Inc. nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

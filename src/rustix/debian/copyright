Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: rustix
Upstream-Contact:
 Dan Gohman <dev@sunfishcode.online>
 Jakub Konka <kubkon@jakubkonka.com>
Source: https://github.com/bytecodealliance/rustix

Files: *
Copyright:
  2020-2022 Dan Gohman <dev@sunfishcode.online>
  2020-2022 Jakub Konka <kubkon@jakubkonka.com>
License: Apache-2.0 WITH LLVM-exception or Apache-2.0 or MIT
Comment:
 Short version for non-lawyers:
 .
 `rustix` is triple-licensed under Apache 2.0 with the LLVM Exception,
 Apache 2.0, and MIT terms.
 .
 .
 Longer version:
 .
 Copyrights in the `rustix` project are retained by their contributors.
 No copyright assignment is required to contribute to the `rustix`
 project.
 .
 Some files include code derived from Rust's `libstd`; see the comments in
 the code for details.
 .
 Except as otherwise noted (below and/or in individual files), `rustix`
 is licensed under:
 .
 - the Apache License, Version 2.0, with the LLVM Exception
   <LICENSE-Apache-2.0_WITH_LLVM-exception> or
   <http://llvm.org/foundation/relicensing/LICENSE.txt>
 - the Apache License, Version 2.0
   <LICENSE-APACHE> or
   <http://www.apache.org/licenses/LICENSE-2.0>,
 - or the MIT license
   <LICENSE-MIT> or
   <http://opensource.org/licenses/MIT>,
 .
 at your option.


Files: debian/*
Copyright:
 2022 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2022 John Goerzen <jgoerzen@complete.org>
License: Apache-2.0 WITH LLVM-exception or Apache-2.0 or MIT

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: Apache-2.0 WITH LLVM-exception
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0
 .
 The additional exception is:
 .
 --- LLVM Exceptions to the Apache 2.0 License ----
 .
 As an exception, if, as a result of your compiling your source code, portions
 of this Software are embedded into an Object form of such source code, you
 may redistribute such embedded portions in such Object form without complying
 with the conditions of Sections 4(a), 4(b) and 4(d) of the License.
 .
 In addition, if you combine or link compiled forms of this Software with
 software that is licensed under the GPLv2 ("Combined Software") and if a
 court of competent jurisdiction determines that the patent provision (Section
 3), the indemnity provision (Section 9) or other Section of the License
 conflicts with the conditions of the GPLv2, you may retroactively and
 prospectively choose to deem waived or otherwise exclude such Section(s) of
 the License, but only in their entirety and only with respect to the Combined
 Software.


License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

#!/usr/bin/make -f

%:
	dh $@ --buildsystem cargo

# regenerating the source code
execute_before_dh_auto_build:
	cp /usr/share/gir-1.0/GLib-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/freetype2-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/Gdk-4.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GdkPixbuf-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GdkX11-4.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/Gio-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/cairo-1.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GModule-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GObject-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/HarfBuzz-0.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/Pango-1.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/PangoCairo-1.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/Gsk-4.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/Graphene-1.0.gir $(CURDIR)
	# the xmlstarlet fixes are taken from upstream here: https://github.com/gtk-rs/gir-files/blob/master/fix.sh
	xmlstarlet ed -L \
	-u '//_:record[@name="PixbufModule"]/_:field[@name="module"]/_:type/@name' -v "gpointer" \
	-u '//_:record[@name="PixbufModule"]/_:field[@name="module"]/_:type/@c:type' -v "gpointer" \
	GdkPixbuf-2.0.gir
	xmlstarlet ed -L \
	-u '//_:class[@name="X11Display"]/_:method[@name="get_xcursor"]//_:type[@name="xlib.Cursor"]/@c:type' -v "gulong" \
	-u '//_:class[@name="X11Display"]/_:method[@name="get_xdisplay"]//_:type[@name="xlib.Display"]/@c:type' -v "gpointer" \
	-u '//_:class[@name="X11Display"]/_:method[@name="get_xrootwindow"]//_:type[@name="xlib.Window"]/@c:type' -v "gulong" \
	-u '//_:class[@name="X11Display"]/_:method[@name="get_xscreen"]//_:type[@name="xlib.Screen"]/@c:type' -v "gpointer" \
	-u '//_:class[@name="X11Monitor"]/_:method[@name="get_output"]//_:type[@name="xlib.XID"]/@c:type' -v "gulong" \
	-u '//_:class[@name="X11Screen"]/_:method[@name="get_monitor_output"]//_:type[@name="xlib.XID"]/@c:type' -v "gulong" \
	-u '//_:class[@name="X11Screen"]/_:method[@name="get_xscreen"]//_:type[@name="xlib.Screen"]/@c:type' -v "gpointer" \
	-u '//_:class[@name="X11Surface"]/_:method[@name="get_xid"]//_:type[@name="xlib.Window"]/@c:type' -v "gulong" \
	-u '//_:class[@name="X11Surface"]/_:function[@name="lookup_for_display"]//_:type[@name="xlib.Window"]/@c:type' -v "gulong" \
	-u '//_:class[@name="get_xid"]/_:method[@name="lookup_for_display"]//_:type[@name="xlib.Window"]/@c:type' -v "gulong" \
	-u '//_:function[@name="x11_get_xatom_by_name_for_display"]//_:type[@name="xlib.Atom"]/@c:type' -v "gulong" \
	-u '//_:function[@name="x11_get_xatom_name_for_display"]//_:type[@name="xlib.Atom"]/@c:type' -v "gulong" \
	-u '//_:function[@name="x11_lookup_xdisplay"]//_:type[@name="xlib.Display"]/@c:type' -v "gpointer" \
	GdkX11-4.0.gir
	xmlstarlet ed -L \
	-u '//*[@glib:error-domain="g-option-context-error-quark"]/@glib:error-domain' -v g-option-error-quark \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_boolean_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "gboolean*" \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_double_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "gdouble*" \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_integer_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "gint*" \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_locale_string_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "const gchar* const*" \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_string_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "const gchar* const*" \
	GLib-2.0.gir
	xmlstarlet ed -L \
	-u '//_:class[@name="Object"]/_:method[@name="getv"]//_:parameter[@name="names"]/_:array/@c:type' -v "const gchar**" \
	-u '//_:class[@name="Object"]/_:method[@name="getv"]//_:parameter[@name="values"]/_:array/@c:type' -v "GValue*" \
	-u '//_:class[@name="Object"]/_:method[@name="setv"]//_:parameter[@name="names"]/_:array/@c:type' -v "const gchar**" \
	-u '//_:class[@name="Object"]/_:method[@name="setv"]//_:parameter[@name="values"]/_:array/@c:type' -v "const GValue*" \
	-u '//_:class[@name="Object"]/_:constructor[@name="new_with_properties"]//_:parameter[@name="names"]/_:array/@c:type' -v "const char**" \
	-u '//_:class[@name="Object"]/_:constructor[@name="new_with_properties"]//_:parameter[@name="values"]/_:array/@c:type' -v "const GValue*" \
	-i '//_:interface[@name="TypePlugin" and not(@glib:type-struct)]' -t 'attr' -n 'glib:type-struct' -v 'TypePluginClass' \
	-i '//_:record[@name="TypePluginClass" and not(@glib:is-gtype-struct-for)]' -t 'attr' -n 'glib:is-gtype-struct-for' -v 'TypePlugin' \
	GObject-2.0.gir
	xmlstarlet ed -L \
	-i '///_:type[not(@name) and @c:type="hb_font_t*"]' -t 'attr' -n 'name' -v "gconstpointer" \
	-u '//_:type[@c:type="hb_font_t*"]/@c:type' -v "gconstpointer" \
	-i '///_:array[not(@name) and @c:type="hb_feature_t*"]' -t 'attr' -n 'name' -v "gconstpointer" \
	-r '///_:array[@c:type="hb_feature_t*"]' -v "type" \
	-d '//_:type[@c:type="hb_feature_t*"]/*' \
	-d '//_:type[@c:type="hb_feature_t*"]/@length' \
	-d '//_:type[@c:type="hb_feature_t*"]/@zero-terminated' \
	-u '//_:type[@c:type="hb_feature_t*"]/@c:type' -v "gconstpointer" \
	-d '//_:record[@c:type="PangoGlyphVisAttr"]/_:field/@bits' \
	-d '//_:record[@c:type="PangoGlyphVisAttr"]/_:field[@name="is_color"]' \
	Pango-1.0.gir
	xmlstarlet ed -L \
	-d '///_:function[@c:identifier="hb_graphite2_face_get_gr_face"]' \
	-d '///_:function[@c:identifier="hb_graphite2_font_get_gr_font"]' \
	-d '///_:function[@c:identifier="hb_ft_face_create"]' \
	-d '///_:function[@c:identifier="hb_ft_face_create_cached"]' \
	-d '///_:function[@c:identifier="hb_ft_face_create_referenced"]' \
	-d '///_:function[@c:identifier="hb_ft_font_create"]' \
	-d '///_:function[@c:identifier="hb_ft_font_create_cached"]' \
	-d '///_:function[@c:identifier="hb_ft_font_create_referenced"]' \
	-d '///_:function[@c:identifier="hb_ft_font_get_face"]' \
	-d '///_:function[@c:identifier="hb_ft_font_lock_face"]' \
	HarfBuzz-0.0.gir
	xmlstarlet ed -L \
	-d '///_:alias[@name="Int32"]' \
	freetype2-2.0.gir
	sed -i 's/girs_directories\s=\s\[\"\.\.\/gir-files\"\]/girs_directories=\[\".\"\]/' $(CURDIR)/Gir.toml
	gir -o .

# Remove the .gir file before install 
execute_before_dh_auto_install:
	rm $(CURDIR)/*.gir
